# Nushell Environment Config File
#
# version = 0.78.0

let nuprompt = $"(ansi black_bold)→"

def create_left_prompt [] {
    mut home = ""
    try {
        if $nu.os-info.name == "windows" {
            $home = $env.USERPROFILE
        } else {
            $home = $env.HOME
        }
    }

    mut user = ""
    try {
        $user = $"[($env.USER)]"
    }

    let dir = ([
        ($env.PWD | str substring 0..($home | str length) | str replace -r $home "~" | str replace -r "~/" "~"),
        ($env.PWD | str substring ($home | str length)..)
    ] | str join)

    let user_color = if (is-admin) {
        $"(ansi red_bold)"
    } else {
        $"(ansi green_bold)"
    }

    $"($user_color)($user)(ansi cyan_bold)[($dir)](ansi magenta)(bash ~/.config/nushell/scripts/git_status.sh) "
}

def create_right_prompt [] {
}

# Use nushell functions to define your right and left prompt
$env.PROMPT_COMMAND = {|| create_left_prompt }
$env.PROMPT_COMMAND_RIGHT = {|| create_right_prompt }

# The prompt indicators are environmental variables that represent
# the state of the prompt
$env.PROMPT_INDICATOR = {|| $"($nuprompt) " }
$env.PROMPT_INDICATOR_VI_INSERT = {|| $"($nuprompt) " }
$env.PROMPT_INDICATOR_VI_NORMAL = {|| $"($nuprompt) " }
$env.PROMPT_MULTILINE_INDICATOR = {|| $"($nuprompt) " }

# Specifies how environment variables are:
# - converted from a string to a value on Nushell startup (from_string)
# - converted from a value back to a string when running external commands (to_string)
# Note: The conversions happen *after* config.nu is loaded
$env.ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
  "Path": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
}

# Directories to search for scripts when calling source or use
#
# By default, <nushell-config-dir>/scripts is added
$env.NU_LIB_DIRS = [
    ($nu.config-path | path dirname | path join 'scripts')
]

# Directories to search for plugin binaries when calling register
#
# By default, <nushell-config-dir>/plugins is added
$env.NU_PLUGIN_DIRS = [
    ($nu.config-path | path dirname | path join 'plugins')
]

# Build-In Aliases & Functions
alias cls = clear

def update-config [] {
    cd ~/.config/nushell
    git pull origin main
}

# RC File
touch ~/.nurc
source ~/.nurc

# To add entries to PATH (on Windows you might use Path), you can use the following pattern:
# $env.PATH = ($env.PATH | split row (char esep) | prepend '/some/path')
