#!/usr/bin/env bash

if git rev-parse --is-inside-work-tree > /dev/null 2>&1; then
	gbranch=$(git branch | cut -f2 -d " ")

	gstatus=""

	gadded="no"

	gwaiting="no"

	waiting_commits=$(git rev-list @{u}..@ --count 2> /dev/null)

	if [[ $waiting_commits -gt 0 ]]; then
		gwaiting="yes"
		gstatus+=" ⇡"
	fi

	first_iter="yes"
	for i in $(git status --porcelain | tac); do
		if [[ "$first_iter" == "yes" ]]; then
			if [[ "$gwaiting" == "no" ]]; then
				if git rev-list @{u}..@ --count > /dev/null 2>&1; then
					gstatus+=" "
				fi
			fi

			first_iter="no"
		fi

		i=$(echo "$i" | sed 's/ *$//; s/^ *//' | cut -f1 -d " ")

		gsymbol=""

		if [[ "$i" == "A"* ]]; then
			if [[ $gadded == "no" ]]; then
				gadded="yes"
				gstatus+="+"
			fi

			i="${i:1}"
		fi

		if [[ "$i" == "??" ]]; then
			gsymbol="?"
		elif [[ "$i" == "M" ]]; then
			gsymbol="!"
		elif [[ "$i" == "D" ]]; then
			gsymbol="✘"
		elif [[ "$i" == "R" ]]; then
			gsymbol=">"
		elif [[ "$i" == "C" ]]; then
			gsymbol=">"
		else
			gsymbol=""
		fi

		if [[ "$gstatus" == *"$gsymbol"* ]]; then
			continue
		else
			gstatus+="$gsymbol"
		fi
	done

	echo "[$gbranch$gstatus]"
else
	echo ""
fi
